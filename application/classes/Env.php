<?php
namespace App\Classes;
class Env{
    public function __construct($p_base_path) {
        $dotEnv=\Dotenv\Dotenv::create($p_base_path );//เรียกคำสั่ง Dotenv
		$dotEnv->load();
	}
}