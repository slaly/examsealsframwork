<?php
require_once('vendor/autoload.php');
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

//การนำmix bootstrap.php มาไว้ในไฟล์นี้ด้วย
$isDevMode = false;
$connectionParams = array( 
 'url'=> 'oci8://seals:seals@localhost/orcl?charset=utf8' //กำหนด url ของการเชื่อมต่อ Oracle
);
$paths = array(__DIR__ . "/application/entities");
$config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
//ORM จะทำงานได้ ต้องมี EntityManager=database และ entity คือ table
$entityManager = EntityManager::create($connectionParams,$config);
return ConsoleRunner::createHelperSet($entityManager);