var gulp = require ('gulp');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS =require('gulp-minify-css');
// var babel = require('gulp-babel');

		
var jsFile = [
	'./resources/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js', 
    './resources/assets/libs/bootstrap/dist/js/bootstrap.min.js',
    './resources/assets/libs/datatables.net/js/jquery.dataTables.min.js',
    './resources/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
    './resources/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js',
    './resources/assets/libs/datatables.net-buttons/js/buttons.flash.min.js',
    './resources/assets/libs/datatables.net-buttons/js/buttons.html5.min.js',
    './resources/assets/libs/datatables.net-buttons/js/buttons.print.min.js',
    './resources/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
    './resources/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js',
    './resources/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js',
    './resources/assets/libs/dropzone/dist/min/dropzone.min.js',
    './resources/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js',
    './resources/assets/libs/jquery/dist/jquery.min.js',
    './resources/assets/libs/jquery-ui/jquery-ui.min.js',
    './resources/assets/libs/jszip/dist/jszip.min.js',
    './resources/assets/libs/moment/min/moment.min.js',
    './resources/assets/libs/pdfmake/build/pdfmake.min.js',
    './resources/assets/libs/pdfmake/build/vfs_fonts.js',
    './resources/assets/libs/summernote/dist/summernote-bs4.min.js',
    './node_modules/flatpickr/dist/flatpickr.min.js',
    

];
var cssFile = [ 
	'./resources/assets/libs/bootstrap/dist/css/bootstrap.min.css',
    './resources/assets/libs/dropzone/dist/min/dropzone.min.css',
    './resources/assets/libs/summernote/dist/summernote-bs4.css',
    './resources/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    './resources/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css',
    './resources/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css',
    './node_modules/flatpickr/dist/flatpickr.min.css'
];




gulp.task ('libjs',function(){
	return gulp
	.src(jsFile) 
	.pipe(gulp.dest('./publication/js'));   //copy to path for security specific of into
});
gulp.task ('libcss',function(){
	return gulp
	.src(cssFile) 
	.pipe(gulp.dest('./publication/css'));   //copy to path for security specific of into
})

//  gulp.task('all',(['libjs']));

