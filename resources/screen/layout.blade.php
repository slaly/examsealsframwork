<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="@relative('css/bootstrap.min.css')">
    {{-- <link rel="stylesheet" href="@relative('css/bootstrap-datetimepicker-standalone.min.css')"> --}}
    <link rel="stylesheet" href="@relative('css/buttons.bootstrap4.min.css')">
    <link rel="stylesheet" href="@relative('css/dataTables.bootstrap4.min.css')">
    <link rel="stylesheet" href="@relative('css/dropzone.min.css')">
    <link rel="stylesheet" href="@relative('css/summernote-bs4.css')">
    <link rel="stylesheet" href="@relative('css/dataTables.bootstrap4.min.css')">
    <link rel="stylesheet" href="@relative('css/flatpickr.min.css')?{{time()}}">

    @yield('style')

</head>
<body>
    <div class="contianer-fluid">
    @yield('content')
    </div>


    <script src="@relative('js/jquery.min.js')"></script>
    <script src="@relative('js/jquery-ui.min.js')"></script>
    <script src="@relative('js/bootstrap.bundle.min.js')"></script> 
    <script src="@relative('js/moment.min.js')"></script>
    <script src="@relative('js/dropzone.min.js')"></script>
    <script src="@relative('js/jquery.inputmask.bundle.min.js')"></script>
    <script src="@relative('js/summernote-bs4.min.js')"></script>
    <script src="@relative('js/jquery.dataTables.min.js')"></script>
    <script src="@relative('js/dataTables.bootstrap4.min.js')"></script>
    {{--  print --}}
    <script src="@relative('js/jszip.min.js')"></script>
    <script src="@relative('js/pdfmake.min.js')"></script>
    <script src="@relative('js/vfs_fonts.js')"></script>
    <script src="@relative('js/dataTables.buttons.min.js')"></script>
    <script src="@relative('js/buttons.bootstrap4.min.js')"></script> 
    <script src="@relative('js/buttons.flash.min.js')"></script>
    <script src="@relative('js/buttons.html5.min.js')"></script>
    <script src="@relative('js/buttons.print.min.js')"></script>
    <script src="@relative('js/dataTables.responsive.min.js')"></script>
    <script src="@relative('js/responsive.bootstrap4.min.js')"></script>
    {{-- <script src="@relative('js/common.min.js')?{{time()}}"></script> --}}
    <script src="@relative('js/flatpickr.min.js')?{{time()}}"></script>
    {{-- <script src="@relative('js/rangPlugin.min.js')?{{time()}}"></script> --}}
  
    {{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script> --}}
  
    
    



   
    @yield('script')

</body>
</html>