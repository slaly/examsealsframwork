<?php

if(!isset($_SESSION)) session_start();

define('BASE_PATH', realpath(__DIR__.'/../'));
date_default_timezone_set("Asia/Bangkok");

/* Auto Loader*/
//require(BASE_PATH . "/app/classes/Env.php");
//$loader->addPsr4('App\\', BASE_PATH . "/app"); //method addPsr4 
$loader = require_once BASE_PATH . '/vendor/autoload.php';

/**
 *  PHP DotENV
 */
new \App\Classes\Env(BASE_PATH);

/**
 * Routing
 */
$router = new AltoRouter();
$router->setBasePath(getenv('APP_PATH'));

require_once BASE_PATH.'/application/routers/web.php';

//print_r($router->match());
new \App\Classes\RoutingDispatcher($router);
